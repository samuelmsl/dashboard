<?php
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "latihan";
$dsn = "mysql:host=$dbhost;dbname=$dbname;charset=utf8mb4";
$pdo = new PDO($dsn, $dbuser, $dbpass);
?>

<html>
	<body>
	<?php
	$stmt = $pdo->query('SELECT * FROM siswa');
	echo '<table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">';
	echo '<thead>';
	echo '<tr role=row>';
	echo '<th>NIS</th>';
	echo '<th>Nama lengkap</th>';
	echo '<th>Tanggal lahir</th>';
	echo '<th>Nilai</th>';
	echo '<th>Jurusan</th>';
	echo '</thead>';

	while ($row = $stmt->fetch()) {
		echo '<tr>';
		echo '<td class="sorting_1">'. $row['nis'] . '</td>';
		echo '<td>'. $row['nama'] . '</td>';
		echo '<td>'. $row['tanggal'] . '</td>';
		echo '<td>'. $row['nilai'] . '</td>';
		echo '<td>'. $row['jurusan'] . '</td>';
		echo '</tr>';
	};
	echo '</table>';
	?>
	</body>
</html>