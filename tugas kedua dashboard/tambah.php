<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tambah</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
          <a class="navbar-brand brand-logo h1" href="index.html">NexSOFT</a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
            
            </li>
            <li class="nav-item nav-profile">
                <a class="nav-link"href="tambah.html">
                  <span class="menu-title">Basic UI Elements</span>
                  <i class="mdi mdi-crosshairs-gps menu-icon"></i>
                </a>
            
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.html">
                    <span class="menu-title">Dashboard</span>
                    <i class="mdi mdi-home menu-icon"></i>
                  </a>
            </li>
            <li class="nav-item">
            </li>
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row" id="proBanner">
          
            <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Daftar Karyawan Baru</h4>
                    <p class="card-description"> Memasukan Data ke Database </p>
                    <form class="forms-sample">
                      <div class="form-group">
                        <label for="exampleInputUsername1">Nama</label>
                        <input type="text" class="form-control" id="nama" placeholder="Nama lengkap">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                        <input type="text" class="form-control" id="tanggal" placeholder="Exp : 1 Januari 1994">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Tempat</label>
                        <input type="text" class="form-control" id="tempat" placeholder="Exp : Jakarta">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Pekerjaan</label>
                        <input type="text" class="form-control" id="pekerjaan" placeholder="Exp : Programmer">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Gaji</label>
                        <input type="text" class="form-control" id="gaji" placeholder="Exp : Rp 3.000.000">
                      </div>
                      <button id ="submitin" type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                      
                    </form>
                  </div>
                </div>
            </div>
            

            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="assets/vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/todolist.js"></script>
    <!-- End custom js for this page -->
  </body>







  


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
    </body>

  <script defer>
    // var name = prompt("what is your name?");
    const socket = new WebSocket("ws://localhost:6363");

    socket.onopen = function () {
    console.log("connect to server");
    };

    var log = document.getElementById("log");

    socket.onmessage = function (event) {
        console.log(event);
        var json = JSON.parse(event.data);
    
        
    };

    document.getElementById("submitin").onclick = function () {
    alert("Berhasil merekam Data");
    
    var nama = document.getElementById("nama").value;
    var tanggal = document.getElementById("tanggal").value;
    var tempat = document.getElementById("tempat").value;
    var pekerjaan = document.getElementById("pekerjaan").value;
    var gaji = document.getElementById("gaji").value;

    socket.send(
        JSON.stringify({
        nama: nama,
        tanggal: tanggal,
        tempat: tempat,
        pekerjaan: pekerjaan,
        gaji: gaji
        })
    );
    // log.innerHTML += "you : " + text + "<br>";
    };
</script>




</html>