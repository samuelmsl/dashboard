package org.nexsoft;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * A simple WebSocketServer implementation. Keeps track of a "chatroom".
 */
public class JavaServer extends WebSocketServer {

    String nama;
    String tanggal;
    String tempat;
    String pekerjaan;
    String gaji;

    public JavaServer(int port ) throws UnknownHostException {
        super( new InetSocketAddress( port ) );
    }

//    public ChatServer(InetSocketAddress address ) {
//        super( address );
//    }

    @Override
    public void onOpen( WebSocket conn, ClientHandshake handshake ) {
        System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " Doi buka webnya!" );

        SQLConnOutput o = new SQLConnOutput();
        o.runSQL();


    }

    @Override
    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
        broadcast( conn + "Doi pergi" );
        System.out.println( conn + "Doi pergi bro!" );
    }

    @Override
    public void onMessage( WebSocket conn, String message ) {
        broadcast( message );
        System.out.println( conn + ": " + message );
        System.out.println(message);
        System.out.println(conn);
        JSONObject myResp = new JSONObject((message));
        System.out.println(myResp.getString("nama"));
        this.nama = myResp.getString("nama");
        this.tanggal = myResp.getString("tanggal");
        this.tempat = myResp.getString("tempat");
        this.pekerjaan = myResp.getString("pekerjaan");
        this.gaji = myResp.getString("gaji");

        SQLConnInput m = new SQLConnInput();
        SQLConnOutput n = new SQLConnOutput();


        m.runSQL(this.nama,this.tanggal,this.tempat,this.pekerjaan,this.gaji);
        System.out.println(n.returnArray());
        ;
//        n.runSQL();
    }

    @Override
    public void onMessage( WebSocket conn, ByteBuffer message ) {
        broadcast( message.array() );
        System.out.println( conn + ": " + message );
    }

    public static void main( String[] args ) throws InterruptedException , IOException {
        int port = 4444;
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }
        JavaServer s = new JavaServer( port );
        s.start();
        System.out.println( "Server started on port: " + s.getPort() );

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            s.broadcast( in );
            if( in.equals( "exit" ) ) {
                s.stop(1000);
                break;
            }
        }
    }
    @Override
    public void onError( WebSocket conn, Exception ex ) {
        ex.printStackTrace();
        if( conn != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        System.out.println("Server started!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }

}