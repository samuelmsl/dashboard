package org.nexsoft;

import java.sql.*;
import java.util.ArrayList;

public class SQLConnOutput {

    private ArrayList<String> thisout = new ArrayList<>();

    public static void main(String[] args) {
        SQLConnOutput newSQL = new SQLConnOutput();
//        newSQL.runSQL();
        System.out.println(newSQL.returnArray());
    }

    public void runSQL() {
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection myConn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/karyawan?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root",""
            );

            Statement stmt = myConn.createStatement();
            ResultSet srs =  stmt.executeQuery("SELECT * FROM data");

            System.out.println("Data berhasil di baca!");
            while (srs.next()) {
                String index = srs.getString("index");
                String nama = srs.getString("nama");
                String tanggal =srs.getString("tanggal");
                String asal = srs.getString("asal");
                String pekerjaan = srs.getString("pekerjaan");
                String gaji = srs.getString("gaji");


                String response = "{\"id\" : \""+ index +"\", \"nama\" : \""+ nama +"\", \"tanggal\" : \""+ tanggal +"\", \"asal\" : \""+ asal +"\", \"pekerjaan\" : \""+ pekerjaan +"\",\"gaji\" : \""+gaji+"\"}";
                thisout.add(response);
                System.out.println(response);

            }

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> returnArray () {
        runSQL();
        return this.thisout;
    }

}
